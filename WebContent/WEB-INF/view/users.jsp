<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <c:if test="${role eq 'ADMIN'}">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Users</title>
</head>
<body>

 
    <h3>User Controll</h3>
 
 
 
    <table border="2" cellpadding="6" cellspacing="3" >
       <tr>
          <th>Login</th>
          <th>Password</th>
          <th>Role</th>
          <th>Status</th>
          <th>Edit</th>
          <th>Delete</th>
       </tr>
       <c:forEach items="${allUser}" var="user" >
          <tr>
             <td>${user.login}</td>
             <td>${user.password}</td>
             <td>${user.role}</td>
             <td>${user.status}</td>
             <td>
                <a href="editUser?login=${user.login}">Edit</a>
             </td>
             <td>
                <a href="deleteUser?login=${user.login}">Delete</a>
             </td>
             
          </tr>
       </c:forEach>
    </table>
  
    
 
    
 

</body>
</html>
</c:if>