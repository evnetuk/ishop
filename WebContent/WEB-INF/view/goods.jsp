<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Goods</title>
</head>
<body>

 
    <h3>Product List</h3>
 
 
 
    <table border="2" cellpadding="6" cellspacing="3" >
       <tr>
          <th>Code</th>
          <th>Name</th>
          <th>Price</th>
          <c:if test="${role eq 'ADMIN'}">
          <th>Edit</th>
          <th>Delete</th>
          </c:if>
          <th>Buy</th>
       </tr>
       <c:forEach items="${allGoods}" var="product" >
          <tr>
             <td>${product.id}</td>
             <td>${product.name}</td>
             <td>${product.price}</td>
             <c:if test="${role eq 'ADMIN'}">
             <td>
                <a href="editProduct?idgoods=${product.id}">Edit</a>
             </td>
             
             <td>
                <a href="deleteProduct?idgoods=${product.id}">Delete</a>
             </td>
             </c:if>
             <td>
                <a href="orderProduct?idgoods=${product.id}&name=${product.name}&price=${product.price}&description=${product.description}">Buy</a>
             </td>
          </tr>
       </c:forEach>
    </table>
   Role:  ${role}
    <c:if test="${role eq 'ADMIN'}">
 <form action="goods"  method="post">
       <p style="color:red">   ${goodsCreatedError}  </p><br>
			Please enter your goods name	
			<input type="text" name="name"/><br>		
		
			Please enter your goods price
			<input type="text" name="price"/><br>
			
			
			<input type="submit" value="submit"/>
			
		</form>
    </c:if>
 
    
 

</body>
</html>