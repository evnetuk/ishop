<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body> <h3>Product Cart List</h3>
 
 
 
    <table border="3" cellpadding="10" cellspacing="3" >
       <tr>
          <th>Code</th>
          <th>Name</th>
          <th>Price</th>
          <th>Delete</th>
          
       </tr>
       <c:forEach items="${cart}" var="product" >
          <tr>
             <td>${product.id}</td>
             <td>${product.name}</td>
             <td>${product.price}</td>
             <td>
                <a href="deleteOrder?idgoods=${product.id}&name=${product.name}&price=${product.price}&description=${product.description}">Remove</a>
             </td>
            
          </tr>
       </c:forEach>
    </table>
    <form action="goods" method="get">
	  	<input type="submit" value="More goods">
		</form>
                  
</body>
</html>