<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div style="padding: 5px;">
 
   <a href="/IShop/home">Home</a>
   |
   <a href="/IShop/goods">Product List</a>
   |
   <a href="/IShop/registerClient">Register</a>
   |
   <a href="/IShop/doLogin">Login</a>
   |   
   <c:if test="${role eq 'ADMIN'}">
    <a href="/IShop/users">Administration</a>
    </c:if>
    
</div>  