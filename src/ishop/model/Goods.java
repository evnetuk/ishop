package ishop.model;

import java.util.LinkedList;
import java.util.Objects;

public class Goods {

	private int id;
	private String name;
	private String description;
	private float price;

	public Goods() {
	}

	public Goods(int id, String name, float price, String description) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.description=description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Goods [id=" + id + ", name=" + name + ", description=" + description + ", price=" + price + "]";
	}

	@Override
	public boolean equals(Object obj) {
if(obj == null) {
	return false;
}
if(!(obj instanceof Goods)) {
	return false;
}
return ((Goods)obj).getId()==this.getId();
	}

	@Override
	public int hashCode() {
	return Objects.hashCode(this.id);
	}

	public static void main(String[] ggg) {
		Goods a1 = new Goods(1,"coffee",12.1f,"black");
		Goods a2 = new Goods(2,"coffee",12.1f,"black");
		LinkedList<Goods> ss = new LinkedList<>();
		ss.add(a1);
		ss.add(a2);
		ss.remove(new Goods(1,"coffee",12.1f,"black"));
 System.out.println("Objects equals=" + (a1.equals(a2)));
System.out.println("Objects == " + (a1==a2));
System.out.println(ss);
	}
	
}