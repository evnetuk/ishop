package ishop.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ishop.exception.IShopException;
import ishop.model.Goods;

public class GoodsDAO {

	public List<Goods> getAll(){
	
		List<Goods> allGoods = new LinkedList<>();
			String getAllQuery = "select * from goods";
			try(Connection conn = ConnUtil.getMySQLConnection();
			PreparedStatement req=conn.prepareStatement(getAllQuery);){
			ResultSet result = req.executeQuery();
			while(result.next()) {
				int goodId = result.getInt("idgoods");
				String name = result.getString("name");
				String description = result.getString("description");
				float price = result.getFloat("price");
				allGoods.add(new Goods(goodId, name, price, description));
			}
			}catch(SQLException ex) {
				ex.printStackTrace();
			}
		
		return allGoods;
		
	}
	public static void deleteProduct(int idgoods) {
		String deleteGoods="delete from goods where idgoods = ?";
		try(Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req=conn.prepareStatement(deleteGoods);){
			req.setInt(1, idgoods);
			req.execute();
		} catch (SQLException e) {
	
			e.printStackTrace();
		}

	}
	public static  Goods selectFromId(int idgoods){	
		String getFromIdQuery = "SELECT name, price, description FROM goods where idgoods=?";
		try(Connection conn = ConnUtil.getMySQLConnection();
		PreparedStatement req=conn.prepareStatement(getFromIdQuery);){
			req.setInt(1,idgoods);
		ResultSet result = req.executeQuery();
		if(result.next()) {
			String name = result.getString("name");
			String description = result.getString("description");
			float price = result.getFloat("price");
			return new Goods( idgoods,name, price, description);
		}
		}catch(SQLException ex) {
			ex.printStackTrace();
		}
	
	return null;
		
	}
	
	public static  void editProduct(int idgoods, String name, float price) {
		String editGoods="update goods set name=?, price=? where idgoods = ?";
		try(Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req=conn.prepareStatement(editGoods);){
			req.setString(1, name);
			req.setFloat(2, price);
			req.setInt(3, idgoods);
			req.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static  void createNewGoods(String name,float price) {
		String insertTableSQL = "INSERT INTO goods"
				+ "( name, price) VALUES"
				+ "(?,?)";
		try(Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req=conn.prepareStatement(insertTableSQL);){
		
		req.setString(1, name);
		req.setFloat(2, price);
		req.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

		
	}

