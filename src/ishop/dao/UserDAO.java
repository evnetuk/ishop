package ishop.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ishop.model.Status;

import ishop.exception.IShopException;
import ishop.model.Goods;
import ishop.model.Role;
import ishop.model.User;

public class UserDAO {
	public List<User> getAll() {
		List<User> allUser = new LinkedList<>();
		String getAllQuery = "select * from user";
		try (Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req = conn.prepareStatement(getAllQuery);) {
			ResultSet result = req.executeQuery();
			while (result.next()) {
				User user = convertToUser(result);
				allUser.add(user);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return allUser;

	}

	private static User convertToUser(ResultSet result) throws SQLException {
		String login = result.getString("login");
		String password = result.getString("password");
		Role role = Role.valueOf(result.getString("role"));
		Status status = Status.valueOf(result.getString("status"));
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		user.setRole(role);
		user.setStatus(status);
		return user;
	}

	public User findUser(String login, String password) {

		String getUserLogin = "SELECT * FROM user WHERE login=? and password=?;";
		User user = null;
		try (Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req = conn.prepareStatement(getUserLogin);) {
			req.setString(1, login);
			req.setString(2, password);
			ResultSet rs = req.executeQuery();
			if (rs.next()) {
				user = convertToUser(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public void registration(String login, String password, String note) throws IShopException {
		String insertTableSQL = "INSERT INTO USER" + "( login, password, note) VALUES" + "(?,?,?)";
		try (Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req = conn.prepareStatement(insertTableSQL);) {
			req.setString(1, login);
			req.setString(2, password);
			req.setString(3, note);
			req.executeUpdate();
		} catch (SQLException e) {
			String errorText = "Internal error.";
			if (e.getMessage().contains("Duplicate")) {
				errorText = "Login '" + login + "' is already exist";
			}
			throw new IShopException(errorText);
		}
	}

	public static void editUser(String login, String password, String status, String role) {
		String editUser = "update user set status=?, password=?, role=? where login = ?";
		try (Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req = conn.prepareStatement(editUser);) {
			req.setString(1, status);
			req.setString(2, password);
			req.setString(3, role);
			req.setString(4, login);
			req.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void deleteUser(String login) {
		String deleteUser = "delete from user where login = ?";
		try (Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req = conn.prepareStatement(deleteUser);) {
			req.setString(1, login);
			req.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static User find(String login) {
		String findByLoginQuery = "SELECT * FROM user where login=?";
		try (Connection conn = ConnUtil.getMySQLConnection();
				PreparedStatement req = conn.prepareStatement(findByLoginQuery);) {
			req.setString(1, login);
			ResultSet result = req.executeQuery();
			if(result.next()) {
				return convertToUser(result);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
