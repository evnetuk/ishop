package ishop.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ishop.dao.UserDAO;
import ishop.exception.IShopException;
import ishop.model.User;

@WebServlet(urlPatterns = { "/users"})
public class UserController extends HttpServlet {

	private static final long serialVersionUID = -3920275190059337942L;
	private UserDAO mapper =  new UserDAO();
	 
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getSession().getAttribute("role")==null) {
			resp.getWriter().print("You are not login in or you have no privileges.  ");
			return;
		}
		req.setAttribute("role", req.getSession().getAttribute("role"));
		req.setAttribute("allUser", mapper.getAll());
		req.getRequestDispatcher("/WEB-INF/view/users.jsp").forward(req,resp);
		
	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
	   
	     doGet(req, resp);
	}
}