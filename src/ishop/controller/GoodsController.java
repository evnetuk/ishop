package ishop.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ishop.dao.GoodsDAO;
import ishop.exception.IShopException;
import ishop.model.Goods;

@WebServlet(urlPatterns = { "/goods"})
public class GoodsController extends HttpServlet {

	private static final long serialVersionUID = -3920275190059337942L;
	private GoodsDAO mapper =  new GoodsDAO();
	 
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getSession().getAttribute("role")==null) {
			resp.getWriter().print("You are not login in");
			return;
		}
		req.setAttribute("role", req.getSession().getAttribute("role"));
		req.setAttribute("allGoods", mapper.getAll());
		req.getRequestDispatcher("/WEB-INF/view/goods.jsp").forward(req,resp);
		
	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	     String name = req.getParameter("name");
	     String prices = req.getParameter("price");
	     if(prices.matches("[0-9.]+")) {
	     float price = Float.parseFloat(prices);
			GoodsDAO.createNewGoods(name, price);
	     }
	     doGet(req, resp);
	}
	



	
}
