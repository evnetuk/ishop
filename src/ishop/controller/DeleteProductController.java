package ishop.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ishop.dao.GoodsDAO;

/**
 * Servlet implementation class EditProduct
 */
@WebServlet("/deleteProduct")
public class DeleteProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private GoodsDAO mapper =  new GoodsDAO();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteProductController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       String code =request.getParameter("idgoods");
       int idgoods = Integer.parseInt(code);
       GoodsDAO.deleteProduct(idgoods);
       
       request.setAttribute("allGoods", mapper.getAll());

       request.getRequestDispatcher("/WEB-INF/view/goods.jsp").forward(request,response);
       
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
