package ishop.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ishop.dao.UserDAO;
import ishop.exception.IShopException;


@WebServlet("/registerClient")
public class RegisterClientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	  private   UserDAO userDAO = new UserDAO();
       

    public RegisterClientController() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("/WEB-INF/view/registerClient.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String login = request.getParameter("login");
	     String password = request.getParameter("password");
	     String address = request.getParameter("note");
	     
	     try {
			userDAO.registration(login, password, address);
		} catch (IShopException e) {
			request.setAttribute("clientRegisterError", e.getMessage());
			request.getRequestDispatcher("/WEB-INF/view/registerClient.jsp").forward(request,response);
		}
	     response.getWriter().print("Registration successful.");
		
	}

}
