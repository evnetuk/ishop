package ishop.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ishop.dao.GoodsDAO;

/**
 * Servlet implementation class EditProductController
 */
@WebServlet("/editProduct")
public class EditProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditProductController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String productId = request.getParameter("idgoods");
		 int idgoods = Integer.parseInt(productId);
		request.setAttribute("product", GoodsDAO.selectFromId(idgoods));
		request.getRequestDispatcher("/WEB-INF/view/editProduct.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code =request.getParameter("idgoods");
		String name=request.getParameter("name");
		String prices=request.getParameter("price");
	       int idgoods = Integer.parseInt(code);
	       float price = Float.parseFloat(prices);
	       GoodsDAO.editProduct(idgoods, name, price);
	       doGet(request, response)	;
	}

}
