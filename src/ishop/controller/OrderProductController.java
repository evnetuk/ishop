package ishop.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ishop.model.Goods;

/**
 * Servlet implementation class OrderProductController
 */
@WebServlet("/orderProduct")
public class OrderProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public OrderProductController() {
        super();
     
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String product_id= request.getParameter("idgoods");
		String poduct_name=request.getParameter("name");
		String product_price=request.getParameter("price");
		String product_desc=request.getParameter("description");
			int idgoods=Integer.parseInt(product_id);
		 float price = Float.parseFloat(product_price);
		Goods newGoods = new Goods(idgoods,poduct_name,price,product_desc);
	
	
		List<Goods> cart = (List<Goods>) request.getSession().getAttribute("cart");
		cart.add(newGoods);
		request.setAttribute("cart",cart);
		request.getRequestDispatcher("/WEB-INF/view/orderProduct.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
