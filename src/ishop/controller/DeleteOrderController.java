package ishop.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ishop.model.Goods;


@WebServlet("/deleteOrder")
public class DeleteOrderController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public DeleteOrderController() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String product_id= request.getParameter("idgoods");
		String poduct_name=request.getParameter("name");
		String product_price=request.getParameter("price");
		String product_desc=request.getParameter("description");
			int idgoods=Integer.parseInt(product_id);
		 float price = Float.parseFloat(product_price);
			Goods newGoods = new Goods(idgoods,poduct_name,price,product_desc);
			
			
			List<Goods> cart = (List<Goods>) request.getSession().getAttribute("cart");
			cart.remove(newGoods);
			request.setAttribute("cart",cart);
			request.getRequestDispatcher("/WEB-INF/view/orderProduct.jsp").forward(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
