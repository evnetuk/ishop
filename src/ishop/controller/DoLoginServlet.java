package ishop.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ishop.dao.ConnUtil;
import ishop.dao.UserDAO;
import ishop.model.Goods;
import ishop.model.User;


@WebServlet("/doLogin")
public class DoLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private   UserDAO userDAO = new UserDAO(); 
  
    public DoLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
        	request.getRequestDispatcher("/WEB-INF/view/login.jsp").forward(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user = null;
         
        boolean hasError = false;
        String errorString = null;
 
        if (login == null || password == null
                 || login.length() == 0 || password.length() == 0) {
            hasError = true;
            errorString = "Required username and password!";
        } else {
            
        	user =userDAO.findUser(login,password);                 
                if (user == null) {
                    hasError = true;
                    errorString = "User Name or password invalid";
                }
        }
        
        // If error, forward to /WEB-INF/views/login.jsp
        if (hasError) { 
        	request.setAttribute("loginError", errorString);
        	request.getRequestDispatcher("/WEB-INF/view/login.jsp").forward(request,response);
    }
        request.getSession().setAttribute("cart",  new LinkedList<>());
        request.getSession().setAttribute("login",login);
        request.getSession().setAttribute("role",user.getRole().name());
        request.getRequestDispatcher("/WEB-INF/view/home.jsp").forward(request,response);
	}


	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().invalidate();
	}
	
}
